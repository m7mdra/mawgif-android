package m7mdra.com.mawgif.presentation.home.avaliable


import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.MarkerOptions

import m7mdra.com.mawgif.R
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import m7mdra.com.mawgif.presentation.common.convertDpToPixel
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_available_places.*
import m7mdra.com.mawgif.domain.places.model.Place
import m7mdra.com.mawgif.presentation.common.log
import m7mdra.com.mawgif.presentation.view.ProgressIndicatorFragment
import javax.inject.Inject


class AvailablePlacesFragment : Fragment(), OnMapReadyCallback, AvailablePlacesView {


    private lateinit var mMap: GoogleMap
    private var supportMapFragment: SupportMapFragment? = null
    private var bottomSheetBehavior: BottomSheetBehavior<*>? = null
    @Inject
    lateinit var presenter: AvailablePlacesPresenter
    private val progressIndicatorFragment = ProgressIndicatorFragment()

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        presenter.loadPlaces()
        googleMap.setOnMarkerClickListener {
            showBottomSheet(presenter.getPlaceById(it.tag as Int))
            true
        }


    }

    private fun showBottomSheet(placeById: Place?) {
        bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
        placeById?.apply {
            place_address.text = address
            place_rent_rate.text = cost.toString()
            place_size.text="$size"
            place_description.text=description
            date.text=createdAt.toString()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_available_places, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        bottomSheetBehavior?.isHideable = true
        bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN

        val fm = activity?.supportFragmentManager
        if (supportMapFragment == null) {
            supportMapFragment = SupportMapFragment.newInstance()
            fm?.beginTransaction()?.replace(R.id.map, supportMapFragment)?.commit()
        } else
            supportMapFragment = fm?.findFragmentById(R.id.map) as SupportMapFragment
        supportMapFragment?.getMapAsync(this)

    }

    override fun onPlacesLoaded(places: List<Place>) {
        loadPlacesIntoMap(places)
        loadPlacesIntoBottomSheet(places)
    }

    override fun toggleProgressIndicator(show: Boolean) {
        if (show)
            progressIndicatorFragment.show(fragmentManager, "")
        else
            progressIndicatorFragment.dismiss()
    }

    override fun showError(errorResId: Int) {
        log(getString(errorResId))
    }

    override fun showError(errorMessage: String) {
        log(errorMessage)

    }

    override fun showSessionExpired() {
        log("Session Expired")

    }

    override fun showNetworkError() {
        log("Network Error")

    }

    override fun showTimeoutMessage() {
        log("Timeout")

    }

    private fun loadPlacesIntoBottomSheet(places: List<Place>) {

    }

    private fun loadPlacesIntoMap(places: List<Place>) {
        log("Loading ${places.size} places into the map")
        val latLngBounds = LatLngBounds.builder()
        places.forEach {
            val latLng = LatLng(it.latitude, it.longitude)
            val markerOptions = MarkerOptions()
                    .position(latLng)
                    .title("${it.address}\n${it.description}")
            val marker = mMap.addMarker(markerOptions)
            marker.tag = it.placeId
            latLngBounds.include(latLng)

        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds.build(), 10))
    }


}
