package m7mdra.com.mawgif.presentation.login

import io.reactivex.disposables.CompositeDisposable
import m7mdra.com.mawgif.domain.user.LoginUseCase
import m7mdra.com.mawgif.presentation.common.Presenter
import m7mdra.com.mawgif.presentation.common.compose
import javax.inject.Inject

class LoginPresenter @Inject constructor(private val loginUseCase: LoginUseCase) : Presenter<LoginView> {
    private lateinit var view: LoginView
    private val compositeDisposable = CompositeDisposable()

    override fun attachView(view: LoginView) {
        this.view = view
    }

    fun loginUser(email: String, password: String) {
        view.toggleProgressIndicator(true)
        val subscribe = loginUseCase.execute(LoginUseCase.Request(email, password))
                .compose()
                .doFinally { view.toggleProgressIndicator(false) }
                .subscribe(/*onSuccess=*/{

                    view.onLoginSuccess()
                },/*onError=*/{
                    view.onLoginFailed()
                })
        compositeDisposable.add(subscribe)
    }


    override fun detachView() {
        compositeDisposable.clear()
    }

}


