package m7mdra.com.mawgif.presentation.common

import android.content.Context
import android.util.Log
import android.widget.TextView
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import android.util.DisplayMetrics


fun <T> Single<T>.compose(): Single<T> {
    return this.compose { it.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()) }
}


fun TextView.text(): String {
    return this.text.toString()
}

fun log(any: Any) {
    Log.d("MEGA", any.toString())
}

fun convertPixelsToDp(px: Float, context: Context): Float {
    val resources = context.resources
    val metrics = resources.displayMetrics
    return px / (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

fun convertDpToPixel(dp: Float, context: Context): Float {
    val resources = context.resources
    val metrics = resources.displayMetrics
    return dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}
