package m7mdra.com.mawgif.presentation.register

import android.util.Log
import io.reactivex.disposables.CompositeDisposable
import m7mdra.com.mawgif.domain.user.CreateAccountUseCase
import m7mdra.com.mawgif.presentation.common.Presenter
import m7mdra.com.mawgif.presentation.common.compose
import m7mdra.com.mawgif.presentation.common.log
import javax.inject.Inject

class RegisterPresenter @Inject constructor(private val createAccountUseCase: CreateAccountUseCase) : Presenter<RegisterView> {
    private lateinit var view: RegisterView
    private val compositeDisposable = CompositeDisposable()

    override fun attachView(view: RegisterView) {
        this.view = view
    }

    fun createAccount(name: String, phone: String, password: String, email: String) {
        view.toggleProgressIndicator(true)
        val subscribe = createAccountUseCase.execute(CreateAccountUseCase.Request(phone, name, email, password))
                .doFinally {
                    view.toggleProgressIndicator(false)
                }
                .compose()
                .subscribe({ view.onRegisterSuccess() }, {
                    view.onRegisterFailed()
                    log(it)
                })
        compositeDisposable.add(subscribe)
    }

    override fun detachView() {
        compositeDisposable.clear()
    }

}
