package m7mdra.com.mawgif.presentation.login

import m7mdra.com.mawgif.presentation.common.BaseView

interface LoginView : BaseView {
    fun onLoginSuccess()
    fun onLoginFailed()
}