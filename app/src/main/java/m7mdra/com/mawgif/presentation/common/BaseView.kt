package m7mdra.com.mawgif.presentation.common

import android.support.annotation.StringRes

interface BaseView {
    fun toggleProgressIndicator(show: Boolean)
    fun showError(@StringRes errorResId: Int)
    fun showError(errorMessage: String)
    fun showSessionExpired()
    fun showNetworkError()
    fun showTimeoutMessage()
}