package m7mdra.com.mawgif.presentation.register

import dagger.Module
import dagger.Provides
import m7mdra.com.mawgif.data.MawgifApi
import m7mdra.com.mawgif.data.user.UserApi
import m7mdra.com.mawgif.data.common.ApiErrorResponse
import m7mdra.com.mawgif.domain.user.CreateAccountUseCase
import m7mdra.com.mawgif.domain.user.UserRepository
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit

@Module
class RegisterActivityModule {

    @Provides
    fun providePresenter(createAccountUseCase: CreateAccountUseCase) = RegisterPresenter(createAccountUseCase)

    @Provides
    fun provideUseCase(userRepository: UserRepository) = CreateAccountUseCase(userRepository)

    @Provides
    fun provideUserRepository(user: MawgifApi.User, errorConverter: Converter<ResponseBody, ApiErrorResponse>):UserRepository =
            UserApi(user, errorConverter)
    @Provides
    fun provideUserApiService(retrofit: Retrofit): MawgifApi.User = retrofit.create(MawgifApi.User::class.java)

}
