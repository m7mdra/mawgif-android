package m7mdra.com.mawgif.presentation.login

import dagger.Module
import dagger.Provides
import m7mdra.com.mawgif.data.MawgifApi
import m7mdra.com.mawgif.data.user.UserApi
import m7mdra.com.mawgif.data.common.ApiErrorResponse
import m7mdra.com.mawgif.domain.user.LoginUseCase
import m7mdra.com.mawgif.domain.user.UserRepository
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit


@Module
class LoginActivityModule {

    @Provides
    fun providePresenter(loginUseCase: LoginUseCase): LoginPresenter = LoginPresenter(loginUseCase)

    @Provides
    fun provideLoginUseCase(userRepository: UserRepository): LoginUseCase = LoginUseCase(userRepository)

    @Provides
    fun provideRepository(userService: MawgifApi.User,
                          errorConverter: Converter<ResponseBody, ApiErrorResponse>): UserRepository =
            UserApi(userService, errorConverter)

    @Provides
    fun provideUserApiService(retrofit: Retrofit): MawgifApi.User = retrofit.create(MawgifApi.User::class.java)


}
