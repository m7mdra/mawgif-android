package m7mdra.com.mawgif.presentation.register

import m7mdra.com.mawgif.presentation.common.BaseView

interface RegisterView : BaseView {
    fun onRegisterSuccess()
    fun onRegisterFailed()
}