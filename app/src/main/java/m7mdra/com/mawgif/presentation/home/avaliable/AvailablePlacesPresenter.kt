package m7mdra.com.mawgif.presentation.home.avaliable

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.disposables.CompositeDisposable
import m7mdra.com.mawgif.R
import m7mdra.com.mawgif.data.common.*
import m7mdra.com.mawgif.domain.places.AvailablePlacesUseCase
import m7mdra.com.mawgif.domain.places.model.Place
import m7mdra.com.mawgif.presentation.common.Presenter
import m7mdra.com.mawgif.presentation.common.compose
import java.net.SocketException


class AvailablePlacesPresenter(private val useCase: AvailablePlacesUseCase) : Presenter<AvailablePlacesView> {
    private lateinit var view: AvailablePlacesView
    private val compositeDisposable = CompositeDisposable()
    private val placeList: MutableList<Place> = mutableListOf()
    override fun attachView(view: AvailablePlacesView) {
        this.view = view
    }

    fun loadPlaces() {
        view.toggleProgressIndicator(true)
        val subscribe = useCase.execute(AvailablePlacesUseCase.Request())
                .doFinally { view.toggleProgressIndicator(false) }
                .compose().subscribe({
                    view.onPlacesLoaded(it)
                    placeList.clear()
                    placeList.addAll(it)
                }, ::handlerError)
        compositeDisposable.add(subscribe)
    }

    fun getPlaceById(placeId: Int): Place? {
        var place: Place? = null
        placeList.forEach {
            if (it.placeId == placeId)
                place = it
        }
        return place
    }

    private fun handlerError(e: Throwable?) {
        e?.printStackTrace()
        return when (e) {
            is TimeoutConnectionException -> view.showTimeoutMessage()
            is ClientConnectionException -> view.showError(R.string.error_no_internet)
            is ApiException -> view.showError(R.string.server_side_error_message)
            is UnAuthorizedException -> view.showSessionExpired()
            is SocketException -> view.showNetworkError()
            is ConnectionException -> view.showNetworkError()
            else -> view.showError(R.string.unknown_error)

        }
    }


    override fun detachView() {
        compositeDisposable.clear()
    }

}