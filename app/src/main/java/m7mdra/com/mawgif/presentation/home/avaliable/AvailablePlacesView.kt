package m7mdra.com.mawgif.presentation.home.avaliable

import m7mdra.com.mawgif.domain.places.model.Place
import m7mdra.com.mawgif.presentation.common.BaseView

interface AvailablePlacesView : BaseView {
    fun onPlacesLoaded(places: List<Place>)
}