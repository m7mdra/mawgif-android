package m7mdra.com.mawgif.presentation.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.TextView
import android.widget.Toast
import dagger.android.AndroidInjection
import io.reactivex.Single
import kotlinx.android.synthetic.main.activity_login.*
import m7mdra.com.mawgif.R
import m7mdra.com.mawgif.presentation.listener.TextValidator
import m7mdra.com.mawgif.presentation.register.RegisterActivity
import m7mdra.com.mawgif.presentation.view.ProgressIndicatorFragment
import timber.log.Timber
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), LoginView {
    override fun showTimeoutMessage() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val progressIndicator = ProgressIndicatorFragment()
    override fun onLoginSuccess() {
        Toast.makeText(this, "Login success", Toast.LENGTH_SHORT).show()

    }

    override fun onLoginFailed() {
        Toast.makeText(this, "Login failed", Toast.LENGTH_SHORT).show()

    }

    override fun toggleProgressIndicator(show: Boolean) {
        progressIndicator.apply {
            if (show) show(supportFragmentManager, "")
            else dismiss()
        }
    }

    override fun showError(errorResId: Int) {

    }

    override fun showError(errorMessage: String) {
    }

    override fun showSessionExpired() {
    }

    override fun showNetworkError() {
    }

    var isPasswordValid = false
    var isEmailValid = false
    @Inject
    lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_login)
        presenter.attachView(this)
        emailTextField.addTextChangedListener(object : TextValidator(emailTextField) {
            override fun validate(textView: TextView, text: String, length: Int) {
                val rule = length != 0 && Patterns.EMAIL_ADDRESS.matcher(text).matches()
                isEmailValid = rule
                loginButton.isEnabled = rule && isPasswordValid
                if (rule) {
                    emailTextInputLayout.error = null
                    emailTextInputLayout.isErrorEnabled=false
                } else {
                    emailTextInputLayout.isErrorEnabled=true
                    emailTextInputLayout.error = getString(R.string.email_error_message)

                }

            }
        })
        passwordTextField.addTextChangedListener(object : TextValidator(passwordTextField) {
            override fun validate(textView: TextView, text: String, length: Int) {
                val rule = length != 0 && length >= 6
                isPasswordValid = rule
                loginButton.isEnabled = rule && isEmailValid
                if (rule) {
                    passwordTextInputLayout.error = null
                    passwordTextInputLayout.isErrorEnabled=false

                } else {
                    passwordTextInputLayout.isErrorEnabled=true
                    passwordTextInputLayout.error = getString(R.string.password_error_message)

                }
            }
        })
        loginButton.setOnClickListener {
            presenter.loginUser(emailTextField.text.toString(), passwordTextField.text.toString())

        }
        buttonOpenRegister.setOnClickListener {
            startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
        }
    }

    override fun onStop() {
        presenter.detachView()
        super.onStop()
    }
}
