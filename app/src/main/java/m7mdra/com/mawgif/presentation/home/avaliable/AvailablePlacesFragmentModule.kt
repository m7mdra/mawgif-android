package m7mdra.com.mawgif.presentation.home.avaliable

import dagger.Module
import dagger.Provides
import m7mdra.com.mawgif.data.MawgifApi
import m7mdra.com.mawgif.data.common.ApiErrorResponse
import m7mdra.com.mawgif.data.place.PlaceApi
import m7mdra.com.mawgif.domain.places.AvailablePlacesUseCase
import m7mdra.com.mawgif.domain.places.PlaceRepository
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit

@Module
class AvailablePlacesFragmentModule {
    @Provides
    fun providePresenter(useCase: AvailablePlacesUseCase) = AvailablePlacesPresenter(useCase)

    @Provides
    fun provideFetchDataUseCase(placeRepository: PlaceRepository): AvailablePlacesUseCase =
            AvailablePlacesUseCase(placeRepository)

    @Provides
    fun provideAvailablePlaceRepository(placeApi: MawgifApi.Places, errorConverter: Converter<ResponseBody, ApiErrorResponse>)
            : PlaceRepository = PlaceApi(placeApi, errorConverter)

    @Provides
    fun provideApiService(retrofit: Retrofit): MawgifApi.Places = retrofit.create(MawgifApi.Places::class.java)
}
