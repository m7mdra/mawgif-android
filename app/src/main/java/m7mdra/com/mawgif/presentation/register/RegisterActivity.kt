package m7mdra.com.mawgif.presentation.register

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.TextView
import android.widget.Toast
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_register.*
import m7mdra.com.mawgif.presentation.login.LoginActivity
import m7mdra.com.mawgif.R
import m7mdra.com.mawgif.presentation.common.text
import m7mdra.com.mawgif.presentation.listener.TextValidator
import m7mdra.com.mawgif.presentation.view.ProgressIndicatorFragment
import javax.inject.Inject

class RegisterActivity : AppCompatActivity(), RegisterView {
    override fun showTimeoutMessage() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val progressIndicator = ProgressIndicatorFragment()

    override fun onRegisterSuccess() {
        Toast.makeText(this, "Register success", Toast.LENGTH_SHORT).show()

    }

    override fun onRegisterFailed() {
        Toast.makeText(this, "Register failed", Toast.LENGTH_SHORT).show()

    }

    override fun toggleProgressIndicator(show: Boolean) {
        progressIndicator.apply {
            if (show) show(supportFragmentManager, "")
            else dismiss()
        }
    }

    override fun showError(errorResId: Int) {
    }

    override fun showError(errorMessage: String) {
    }

    override fun showSessionExpired() {
    }

    override fun showNetworkError() {
    }

    var isNameValid = false
    var isEmailValid = false
    var isPhoneValid = false
    var isPasswordValid = false
    @Inject
    lateinit var presenter: RegisterPresenter

    companion object {
        const val TAG = "MEGA"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_register)
        emailTextField.addTextChangedListener(object : TextValidator(emailTextField) {
            override fun validate(textView: TextView, text: String, length: Int) {
                val rule = length != 0 && Patterns.EMAIL_ADDRESS.matcher(text).matches()
                isEmailValid = rule
                createAccountButton.isEnabled = rule && isPhoneValid && isPasswordValid && isNameValid
                Log.d(TAG, "is name valid? $isNameValid | is password valid? $isPasswordValid | is email valid? $isEmailValid | is phone valid? $isPhoneValid")
                if (rule) {
                    emailTextInputLayout.error = null
                    emailTextInputLayout.isErrorEnabled = false
                } else {
                    emailTextInputLayout.isErrorEnabled = true
                    emailTextInputLayout.error = getString(R.string.email_error_message)
                }
            }
        })
        presenter.attachView(this)
        nameTextField.addTextChangedListener(object : TextValidator(nameTextField) {
            override fun validate(textView: TextView, text: String, length: Int) {
                Log.d(TAG, "is name valid? $isNameValid | is password valid? $isPasswordValid | is email valid? $isEmailValid | is phone valid? $isPhoneValid")
                val rule = length != 0
                isNameValid = rule
                createAccountButton.isEnabled = rule && isEmailValid && isPasswordValid && isPhoneValid
                if (rule) {
                    nameTextInputLayout.error = null
                    nameTextInputLayout.isErrorEnabled = false
                } else {
                    nameTextInputLayout.isErrorEnabled = true
                    nameTextInputLayout.error = getString(R.string.name_error_message)
                }
            }
        })
        phoneTextField.addTextChangedListener(object : TextValidator(phoneTextField) {
            override fun validate(textView: TextView, text: String, length: Int) {
                Log.d(TAG, "is name valid? $isNameValid | is password valid? $isPasswordValid | is email valid? $isEmailValid | is phone valid? $isPhoneValid")
                val rule = length != 0 && text.startsWith("09") || text.startsWith("01")
                isPhoneValid = rule
                createAccountButton.isEnabled = rule && isEmailValid && isPasswordValid && isNameValid
                if (rule) {
                    phoneTextInputLayout.error = null
                    phoneTextInputLayout.isErrorEnabled= false
                } else {
                    phoneTextInputLayout.isErrorEnabled= true
                    phoneTextInputLayout.error = getString(R.string.phone_number_error_message)
                }
            }
        })
        passwordTextField.addTextChangedListener(object : TextValidator(passwordTextField) {
            override fun validate(textView: TextView, text: String, length: Int) {
                Log.d(TAG, "is name valid? $isNameValid | is password valid? $isPasswordValid | is email valid? $isEmailValid | is phone valid? $isPhoneValid")
                val rule = length != 0 && length >= 6
                isPasswordValid = rule
                createAccountButton.isEnabled = rule && isPhoneValid && isNameValid && isEmailValid
                if (rule) {
                    passwordTextInputLayout.error = null
                    passwordTextInputLayout.isErrorEnabled= false
                } else {
                    passwordTextInputLayout.isErrorEnabled= true
                    passwordTextInputLayout.error = getString(R.string.password_error_message)
                }
            }
        })
        buttonOpenLogin.setOnClickListener {
            startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
            finish()
        }
        createAccountButton.setOnClickListener {
            presenter.createAccount(nameTextField.text(), phoneTextField.text(), passwordTextField.text(), emailTextField.text())
        }

    }

    override fun onStop() {
        presenter.detachView()
        super.onStop()
    }
}
