package m7mdra.com.mawgif.presentation.home

import dagger.Module
import dagger.android.ContributesAndroidInjector
import m7mdra.com.mawgif.di.PerFragment
import m7mdra.com.mawgif.presentation.home.avaliable.AvailablePlacesFragment
import m7mdra.com.mawgif.presentation.home.avaliable.AvailablePlacesFragmentModule

@Module
abstract class MainActivityFragmentModuleBuilder {

    @PerFragment
    @ContributesAndroidInjector(modules = [AvailablePlacesFragmentModule::class])
    abstract fun availablePlacesFragment(): AvailablePlacesFragment
}
