package m7mdra.com.mawgif.di

import android.content.Context
import dagger.Module
import dagger.Provides
import m7mdra.com.mawgif.MawgifApp
import javax.inject.Singleton

@Module
class ApplicationModule {
    @Provides
    @Singleton
    fun provideContext(app: MawgifApp): Context = app.applicationContext
}