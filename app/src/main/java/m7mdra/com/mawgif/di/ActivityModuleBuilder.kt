package m7mdra.com.mawgif.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import m7mdra.com.mawgif.presentation.home.MainActivity
import m7mdra.com.mawgif.presentation.home.MainActivityFragmentModuleBuilder
import m7mdra.com.mawgif.presentation.login.LoginActivity
import m7mdra.com.mawgif.presentation.login.LoginActivityModule
import m7mdra.com.mawgif.presentation.register.RegisterActivity
import m7mdra.com.mawgif.presentation.register.RegisterActivityModule

@Module
abstract class ActivityModuleBuilder {

    @PerActivity
    @ContributesAndroidInjector(modules = [LoginActivityModule::class])
    abstract fun loginActivity(): LoginActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [RegisterActivityModule::class])
    abstract fun registerActivity(): RegisterActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [MainActivityFragmentModuleBuilder::class])
    abstract fun mainActivity(): MainActivity
}
