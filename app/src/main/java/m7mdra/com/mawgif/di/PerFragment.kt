package m7mdra.com.mawgif.di


import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerFragment