package m7mdra.com.mawgif.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import m7mdra.com.mawgif.MawgifApp
import javax.inject.Singleton

@Component(modules = [
    AndroidInjectionModule::class,
    AndroidSupportInjectionModule::class,
    ApplicationModule::class,
    NetworkModule::class,
    ActivityModuleBuilder::class])
@Singleton
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: MawgifApp): Builder

        fun build(): AppComponent

    }

    fun inject(app: MawgifApp)
}